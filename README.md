Shopfy Stock Tag Update App

The client required discount codes to only apply to products that were not on sale. As standard, Shopify did not offer this functionality!

I was drafted into this project two days before launch to find a solution. After a brief consulation I decided the best way forward was to
build a custom app that uses the Shopify REST API and Shopify Webhooks to add a tag identifying wether a product was on sale or not based
on its prices whenever its updated. It needed to run on demand, so 'Product Update' webhooks were used to achieve this.

By adding a tag to identify wether a product is full price or on sale, I could then apply custom rules to discount codes to exclude sale items,
solving the customers issue.

This solution had to be built within a single day to meet the tight deadlines I was faced with. I was able to review the issue, research options
and deliver a working solution within this time which allowed the client to launch without delay. I ran the script against 15k products a day
before launch and it ran sucessfully without issue or rate limit restriction.

The solution has two parts;

1. on Shopfiys 'Product Update' Webhook, get the current product price, check its 'Sale' state, and only update the tags if required.

2. run the tag updates against 15k+ products, taking into consideration Shopify API rate limits and tracking failures.


Given additioonal time, I would look at improving data retention as well as looking to optimise the tag update function itself, utlising queues over
running jobs directly to avoid bottlenecks.

