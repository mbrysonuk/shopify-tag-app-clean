<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::middleware('auth:api')->get('/pbreturnuri', '');
//shopify
Route::post('/pb-updateproduct', 'ApiController@product_update')->middleware('shopify');

// Route::post('/pb-updateproducttest', 'ApiController@forward_req');

