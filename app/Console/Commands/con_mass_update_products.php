<?php

namespace App\Console\Commands;
use App\Jobs\mass_update_products;
use Illuminate\Console\Command;

class con_update_products extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blueocto:mass_update_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the products found using grab';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //call job
        //dispatch( new mass_update_products() );
    }
}
