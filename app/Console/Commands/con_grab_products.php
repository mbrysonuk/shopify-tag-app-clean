<?php

namespace App\Console\Commands;
use App\Jobs\grab_products;
use Illuminate\Console\Command;

class con_grab_products extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blueocto:grab_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'grab products from PB JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //call job
        dispatch( new grab_products() );
        
    }
}
