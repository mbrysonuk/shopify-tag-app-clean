<?php

namespace App\Http\Middleware;

use Closure;

class CheckShopifyWebhook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($Request, Closure $next)
    {
        $known_hmac = env('S_WEBHOOK_HMAC', false);
        $shopify_hmac = $Request->header('x-shopify-hmac-sha256');

        if(empty($shopify_hmac)) {
            abort(403, 'Invalid Headers');
        }

        $hmac_header = $shopify_hmac;
        $data = file_get_contents('php://input');
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $known_hmac, true));
        $result = hash_equals($hmac_header, $calculated_hmac);


        if($result) {
            return $next($Request);
        }else{
            abort(403, 'No Key Provided');
        }

        return $next($request);
    }
}
