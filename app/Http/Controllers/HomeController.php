<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\grab_products;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $grab_prods = new grab_products();
        // die();
        //return view('home');
    }
}
