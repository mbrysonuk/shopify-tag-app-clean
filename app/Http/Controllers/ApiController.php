<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\update_product;
use Storage;
use Mail;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('shopify');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function product_update(Request $Request)
    {

        $get_first = function($x){
            return $x[0];
        };
        // Same as getallheaders(), just with lowercase keys
        //implode(",",array_map($get_first, $request->headers->all()));


        // Mail::raw("Product Update Request: \n" . $Request->header('x-shopify-hmac-sha256') . "\n\n" . $Request->getContent() . "\n\n", function ($message) {
        //   $message->to("matthew@blueocto.co.uk")
        //     ->from('noreply@blueocto.co.uk')
        //     ->subject("Product Update Request - Shopify Tag App");
        // });

        $product_json = $Request->getContent();
        $product_data = json_decode($product_json);
        echo "p_type    |   Sale    |   Status  |   Product |   Title\n";
        echo "<br>";
        $result = dispatch(new update_product($product_data));

        return 'Complete';
    }

    public function forward_req(Request $Request)
    {
        //$product_json = $Request->getContent()
        // echo 'storing...';
        // echo 'complete';
        die();
    }
}
