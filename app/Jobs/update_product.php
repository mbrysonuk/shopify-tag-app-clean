<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\product;
use Storage;
use Mail;
use App\productupdatelog;

class update_product implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @params $this->product - a json array of a single product from Shopify
     */
    public function handle()
    {

        //get the products information via the api?? maybe use the webhook data?!
        //run a check to see if the product has been updated in the logs within the last hour. if so, skip.
        $log_item = '';
        $is_sale = false;
        $has_ftag = false;
        $update_required = "false";
        $fp_tag = ' full_price';
        $product_id = $this->product->id;
        $tags = $this->product->tags;
        $variants = $this->product->variants;

        //before we start, lets open a record in the db log
        if(empty(product::where('product_id', $product_id)->first()) ) {
            $log_item = $log_item .  "create   ";
            $db_log                 = new product();
            $db_log->updated        = date('Y-m-d H:i:s');
        }else{
            $log_item = $log_item .  "update  ";
            $db_log = product::where('product_id', $product_id)->first();
        }

        $db_log->product_name   = $this->product->title;
        $db_log->product_id     = $product_id;
        
        $db_log->status         = 0;
        $db_log->save();
        
        //lets indicate we have started the product update by setting the status to 2 (0 = not started, 1 = complete, 2 = in progress)
        $db_log->status = 2;



        //loop through the variants to get the price and check if its on sale
        foreach($variants as $variant) {
            $price = $variant->price;
            $compare_at_price = $variant->compare_at_price;

            if($price != $compare_at_price && !is_null($compare_at_price) && !empty($compare_at_price) ) {
                $is_sale = true;
                //if we find a sale item, theres no need to continue
                break;
            }
        }

        // check to see if item has full price tag already
        $tag_arr = explode(",",$tags);
        foreach($tag_arr as $tag) {
            echo 'tag: ' . $tag . "<br>";
            if($tag == $fp_tag) {
                $has_ftag = true;
                break;
            }
        }

        //Check to see wether we need to update the product based on the ftag
        if($has_ftag && !$is_sale) {
            //if the item has a full price tag and isnt on sale
            $log_item = $log_item .   "|  FP, NO ACTION  ";
        }else if(!$has_ftag && $is_sale) {
            //item doesnt have a full price, and is on sale
            $log_item = $log_item .   "|  SALE, NO ACTION  ";
        }else if((!$has_ftag && !$is_sale)) {
            //item doesnt have a full price tag, and isnt on sale
            $log_item = $log_item .   "|  FP, ADD TAG  ";
            $update_required = "add";
        }else if(($has_ftag && $is_sale)) {
            //item has a full price tag and is on sale
            $log_item = $log_item .   "|  SA, REMOVE TAG  ";
            $tags = $tags . ', '.$fp_tag;
            $update_required = "remove";
        }
        //check what kind of update require
        if($update_required == "add") {
            //add the tag
            $tags = $tags . ', '.$fp_tag;
        }else if($update_required == "remove") {
            $tag_arr = array_diff($tag_arr, array($fp_tag) );
            $tags = implode(",",$tag_arr);
        }else{
            echo $log_item;
            //no update required, so lets just stop the code now.
            //echo 'no update required';
            
            $logupdate = new productupdatelog();
            $logupdate->product_id = $this->product->title;
            $logupdate->product_title = $this->product->id;
            $logupdate->log_item = $log_item;
            $logupdate->save();

            return 'no update required';
        }

        //construct the data array for the update response
        $data = array(
            "product" => array(
                "id"    => (int)$product_id,
                "tags"  => (string)$tags
            )
        );

        //encode the data array as JSON
        $data_json = json_encode($data);

        //construct the PUT request
        $curl = curl_init();
        $s_apikey = env('S_API_KEY', false);
        $s_password = env('S_PASSWORD', false);
        $s_shop = env('S_SHOP', false);
        $url = 'https://'.$s_apikey.':'.$s_password.'@'.$s_shop.'/admin/products/'.$product_id.'.json';
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        $result = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($httpcode == 200) {
            $log_item = $log_item .   "|  UPLOADED  ";
            $db_log->status  = 1;
            
            $status = true;

        }else{
            $log_item = $log_item .  "|  ERROR  ";
            $db_log->status = 2;
            $status = false;
        }
        //Storage::disk('local')->put('shopifyreq.txt', $Request);
        //productid - status - timestamp
        $date_stamp = date('Y-m-d H:i:s');
        $log_item = $log_item .  "|    " . $date_stamp . "  - " . $this->product->id . "    ";
        $log_item = $log_item .  "|    " . $this->product->title . "    \n";    

        //send an email if error
        if($status != true ) {
            //email if something goes wrong
            Mail::raw("Product update failed for: \n" . $log_item . "\n\n" . json_encode($this->product), function ($message) {
              $message->to("matthew@blueocto.co.uk")
                ->from('noreply@blueocto.co.uk')
                ->subject("URGENT - SHOPIFY TAG APP - PRODUCT UPDATE FAILURE");
            });
            $status_clean = 'ERROR';
        }

        $db_log->updated = $date_stamp;
        $db_log->save();
        echo $log_item;

        $logupdate = new productupdatelog();
        $logupdate->product_id = $this->product->title;
        $logupdate->product_title = $this->product->id;
        $logupdate->log_item = $log_item;
        $logupdate->save();

        return $status;
    }
}
