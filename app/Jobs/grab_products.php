<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\product;
use App\Jobs\update_product;
use Mail;

class grab_products implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //https://EXAMPLESITE.myshopify.com/admin/products.json?published_status=published&fields=id,title,tags&limit=250&page=1
        //while products not blank
        //get products

        $i = 1;
        $products = "not loaded\n";
        echo "p_type	|	Sale	|	Status	|	Product	|	Title\n";
        // product::truncate();
        while(!empty($products) && $i) {
            echo "** working on page: ".$i."------------------------\n";
            
            //$url = 'https://EXAMPLESITE.myshopify.com/admin/products.json?published_status=published&fields=id,title,tags&limit=250&page='.$i;
            //$url = https://{username}:{password}@{shop}.myshopify.com/admin/{resource}.json
            //'blueocto-test.myshopify.com/admin/products.json?fields=id,title,tags&limit=250&page='.$i;

            //REAL DATA
            //https://SOMEID:SOMEID@EXAMPLESITE.myshopify.com/admin/orders.json

            //get products
            $compare_at_found = false;
            $curl = curl_init();
            $s_apikey = env('S_API_KEY', false);
            $s_password = env('S_PASSWORD', false);
            $s_shop = env('S_SHOP', false);
            $url = 'https://'.$s_apikey.':'.$s_password.'@'.$s_shop.'/admin/products.json?fields=id,title,price,compare_at_price,presentment_prices,variants,tags&limit=250&page='.$i;

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $products = curl_exec($curl);
            curl_close($curl);
            $products = json_decode($products, false);

            if(count($products->products) == 0) {
                $products = '';
                Mail::raw("Completed all pages! ".
                    "\nTotal pages found: ".$i
                    ."\nProducts Imported: ".product::count()
                    ."\nProducts Failed: ".product::where('status',2)->count(),
                    function ($message) {
                      $message->to("matthew@blueocto.co.uk")
                        ->from('noreply@blueocto.co.uk')
                        ->subject("SUCESS - SHOPIFY TAG APP - Script Completed");
                }); 
            }else{
                foreach($products->products as $product) {
                    //echo "Checking Product:  ".$product->title."..\n";
                    dispatch( new update_product($product) );
                }
            }


            if(count(Product::where('status',2)->get()) > 49 ) {
                break;
                Mail::raw("Serious failure - more than 50 errors have been reported when reaching page ".$i.", so the script has stopped automatically.", function ($message) {
                  $message->to("matthew@blueocto.co.uk")
                    ->from('noreply@blueocto.co.uk')
                    ->subject("URGENT - SHOPIFY TAG APP - MORE THAN 50 ERRORS REPORTED!");
                });
            }
            //iterate for the pagination on shopfiys json file
            $i++;
        }
    }
}
